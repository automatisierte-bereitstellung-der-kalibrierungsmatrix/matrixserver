package org.unistuttgart.matrixserver;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/qc")
public class QCResource {
	private QCList list = QCList.getInstance();
	
	@GET @Path("/list")
	@Produces("application/json")
	public List<QCElement> getQCElements() {
		return list.listAll();
	}
	
	@GET @Path("list/ids")
	@Produces("text/plain")
	public String getIds() {
		String response = "";
		int size = list.size();
		response += Integer.toString(size) + ";";
		for(int i = 0; i < size; i++) {
			response += Integer.toString(list.getQC(i).getId()) + ";";
		}
		return response;
	}
	
	@GET @Path("/ids/{qcid}")
	@Produces("application/json")
	public Response getQC(@PathParam("qcid") int qcId) {
		QCElement element = list.getQCElement(qcId);
		if(element != null) {
			return Response.ok(element, MediaType.APPLICATION_JSON).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	
	@POST @Path("/ids")
	@Consumes("application/json")
	public Response add(QCElement element) throws URISyntaxException {
		int newId = list.addQCElement(element);
		URI url = new URI("/qc/ids/" + newId);
		return Response.created(url).build();
	}
	
	@PUT @Path("/ids/{qcid}")
	@Consumes("application/json")
	public Response update(@PathParam("qcid") int qcId, QCElement element) {
		element.setId(qcId);
		if(list.updateQCElement(element)) {
			return Response.ok().build();
		} else {
			return Response.notModified().build();
		}
	}
		
	@DELETE @Path("ids/{qcid}")
	public Response deleteQC(@PathParam("qcid") int qcId) {
		if(list.deleteQCElement(qcId)) {
			return Response.ok().build();
		} else {
			return Response.notModified().build();
		}
	}
}
